import java.util.InputMismatchException;
import java.util.Scanner;

class InvalidNumberException extends Exception {
    public InvalidNumberException (String str) {
        super(str);
        System.out.println(str);
        System.exit(-1);
    }
}

public class Main {
    public static void main(String[] args) {
        System.out.println("--Factorial Calculation--");
        System.out.println("Enter a number: ");
        Scanner in = new Scanner(System.in);
        int num = 0;

        try {
            num = in.nextInt();
            if (num <= 0) {
                throw new InvalidNumberException("Cannot compute factorial for values less than 1 ");
            }
        } catch (InputMismatchException e) {
            System.out.println("Enter integers only");
            System.exit(-1);
        } catch (Exception e) {
            System.out.println("Something went wrong. Please try again!");
            System.exit(-1);
        }

        int answer = 1;
//        int counter = 1;

//        while (counter <= num) {
//            answer *= counter;
//            counter++;
//        }

        for (int counter = num; counter > 1; counter--) {
            answer *= counter;
        }

        System.out.println("The factorial of " + num + " is " + answer);
    }
}