package com.zuitt.example;

import java.sql.SQLOutput;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        // Exceptions
        // a problem that arises during the execution of the program
        // it disrupts the normal flow of the program and terminates abnormally

        // Exception Handling
        // refers to managing and catching run-time errors in order to safely run your coe

        Scanner input = new Scanner(System.in);

        int num1 = 0;
        int num2 = 0;


        // try-catch-finally
        try {
            System.out.println("Enter 1st number: ");
            num1 = input.nextInt();
            System.out.println("Enter 2nd number: ");
            num2 = input.nextInt();

            System.out.println("The quotient is: " + num1/num2);

        } catch (ArithmeticException e) {
            System.out.println("Illegal division by zero");
        } catch (InputMismatchException e) {
            System.out.println("Please input numbers only!");
        } catch (Exception e) {
            System.out.println("Something went wrong. Please try again!");
        } finally {
            System.out.println("This will execute no matter what.");
        }
    }
}
